#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\Task3\Task3.cpp"
#include <algorithm>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTestTask3
{		
	TEST_CLASS(UnitTest2)
	{
	public:
		
		TEST_METHOD(TestMethod1Array)
		{
			Data d = handleArray(vector<int>({1,2,3,3,4,5,5,5}));
			Assert::AreEqual(5, d.countDiff);
			Assert::AreEqual(5, d.elem);
		}


		TEST_METHOD(TestMethod2Array)
		{
			Data d = handleArray(vector<int>({ 1 }));
			Assert::AreEqual(1, d.countDiff);
			Assert::AreEqual(1, d.elem);
		}


		TEST_METHOD(TestMethod3Array)
		{
			Data d = handleArray(vector<int>({33,33,33,33,4 }));
			Assert::AreEqual(2, d.countDiff);
			Assert::AreEqual(33, d.elem);
		}

		TEST_METHOD(TestMethod4Array)
		{
			Data d = handleArray(vector<int>({10,9,5,4,7,0 }));
			Assert::AreEqual(6, d.countDiff);
			Assert::AreEqual(10, d.elem);
		}


		TEST_METHOD(TestMethod5Array)
		{
			vector<int> a = vector<int>({ 74,22,74,9,6,22,77,74,0,0,22,55 });
			Data d = handleArray(a);
			Assert::AreEqual(7, d.countDiff);
			Assert::AreEqual(74, d.elem);
		}
	};
}