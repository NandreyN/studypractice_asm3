#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\Task2\Palindrome.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(TestMethod1Palindrome)
		{
			int arr1[1] = { 2 };
			Assert::AreEqual(true, isPalindrome(arr1, 1));

		}
		TEST_METHOD(TestMethod2Palindrome)
		{
			int arr3[3] = { 11,88,11 };
			Assert::AreEqual(true, isPalindrome(arr3, 3));
		}

		TEST_METHOD(TestMethod3Palindrome)
		{
			int arr4[4] = { 0,44,0,44 };
			Assert::AreEqual(false, isPalindrome(arr4, 4));
		}
		TEST_METHOD(TestMethod4Palindrome)
		{
			int arr6[6] = { 1,1,2,2,1,2 };
			Assert::AreEqual(false, isPalindrome(arr6, 6));
		}
		TEST_METHOD(TestMethod5Palindrome)
		{
			int arr7[7] = { 14,2,2,5,3,2,14 };
			Assert::AreEqual(false, isPalindrome(arr7, 7));
		}
		TEST_METHOD(TestMethod6Palindrome)
		{
			int arr3[3] = { 1,1,1 };
			Assert::AreEqual(true, isPalindrome(arr3, 3));
		}
		TEST_METHOD(TestMethod7Palindrome)
		{
			int arr8[8] = { 1,2,3,4,4,3,2,1 };
			Assert::AreEqual(true, isPalindrome(arr8, 8));
		}
	};
	
}