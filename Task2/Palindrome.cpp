#include <iostream>
#include <stdio.h>
using namespace std;


bool isPalindrome(int* arr, int len)
{
	int  res = 0; // 0 == true , other == false
	len--;
	int var1;
	int fInd = 0;
	_asm
	{
		cmp len , 0
		je exit_asm

		mov ebx, len 		 // last index
		mov ecx, len
		inc  ecx
		shr ecx, 1

		cycle:
		mov eax, fInd
		mov edx ,4
		imul edx

		mov edx, arr
		add edx ,eax

		mov eax,[edx]
		mov var1 , eax

		
		mov eax ,4
		imul ebx
		mov edx , arr
		add edx, eax
		mov eax , [edx]
		cmp eax , var1
		jne fls
		dec ebx
		inc fInd
		loop cycle
		jmp exit_asm

			fls:
			mov res ,1
		exit_asm:
	}
	
	return (res == 0)?true:false;
}

int main()
{
	return 0;
}