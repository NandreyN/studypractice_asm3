//1. � ������� n*n �������� ������� �������� i-�� ������ � i-��� �������.
#include <iostream>
using namespace std;

const int n = 5;

void print(int ptr[n][n])
{
	for (int i = 0; i < n;i++)
	{
		for (int j = 0; j < n; j++)
			cout << ptr[i][j] << "  ";
		cout << endl;
	}
}

int main()
{
	int matrix[n][n] = { {1,1,1,1,1} ,
						 {2,2,2,2,2},
						 {3,3,3,3,3},
						 {4,4,4,4,4},
						 {5,5,5,5,5} };
	print(matrix);
	int i;
	cout << "Input i : ";
	cin >> i;
	int buffer[n];
	_asm
	{
		; Row into stack // (i*n + j) i = const 
		; Column into Row //(j*n  + i) i = const
		mov ecx, n
		mov ebx, 0;			index
		push_stack: 
		mov eax, n
			imul i
			add eax, ebx

			; Calculating another index and swaping variables 

			mov edi ,eax
			mov eax , n
			imul ebx
			add eax, i
			mov edx , matrix [eax * 4]
			mov esi, matrix[edi * 4]
			mov matrix [edi * 4], edx
			mov matrix [eax * 4], esi

			inc ebx
		loop push_stack 
	}
	cout << "Changed matrix : " << endl << endl;
	print(matrix);
	return 0;
}