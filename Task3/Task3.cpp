//3. ����� ���������� ������ ������������ �����������. 
//���������� ���������� ��������� ��������� ������� � 
//����� ����� ������������� ��������.

#include <iostream>
#include <vector>
using namespace std;

struct Data
{
	int elem;
	int countDiff;
};

Data handleArray(vector<int> vect)
{
	auto arr = vect.data();
	int len = vect.size();
	int ecxBuff,ad1,ad2;
	// Perform sorting 
	_asm
	{
		mov ecx , len

		loop1:
		mov ecxBuff, ecx
		mov ecx, len
		loop2:
		// get first
		mov eax, 4
		mov edx, ecxBuff
		dec edx
		imul edx
		mov edx, arr
		add edx, eax
		mov ad1, edx
		mov ebx, [edx] // first

			mov eax, 4
			mov edx, ecx
			dec edx
			imul edx
			mov edx, arr
			add edx, eax
			mov eax, [edx]

			cmp eax, ebx
			jge newLoop

			mov [edx], ebx
			mov edx, [ad1]
			mov [edx], eax

		newLoop:
		loop loop2
		mov ecx ,ecxBuff

		extern_loop: loop loop1
	}
	// Sort end
	int counter = 1;

	_asm
	{
		mov ecx, len
		dec ecx 

		cmp ecx, 0
		je exit_asm
		 
		mov esi, 1 // counter
		mov ebx, arr
		mov eax , [ebx]
		push eax

		cycle:

		mov ebx , arr
		mov eax,4
		imul esi
		add ebx ,eax
		mov eax , [ebx]

		pop edx
		cmp eax , edx
		je nxt
		inc counter

		nxt:
		push eax
		inc esi
		loop cycle
		pop ebx
		
			exit_asm:
	}
	Data d;
	d.countDiff = counter;
	counter = 0;
	int element;

	_asm
	{
		mov esi , 0
		mov ebx, arr
		mov eax , [ebx]
		mov element , eax
		mov counter , 1	

		cmp len , 1
		je finish

		dec len
		cycle2:
		inc esi
		mov eax, 4
		imul esi
		add ebx, eax
		mov edx, [ebx]
		sub ebx, eax

		cmp edx, element 
		jne not_eq			
		inc counter
		jmp index_check

		not_eq:
		// push : <top> element | index<bottom>
		push counter
		push element
		mov element , edx
		mov counter , 1

		index_check: cmp esi, len
		jl cycle2
		inc len
		// While finished. There are d.countDiff pairs of values in stack
				mov ecx, d.countDiff
			dec ecx
			loop3:
		pop eax // elem
		pop ebx // ind

		cmp ebx , counter
		jle next_iteration
		mov element , eax
		mov counter , ebx
			next_iteration :
		loop  loop3
			finish:
		
	}
	d.elem = element;
	return d;
}


int main()
{
	handleArray(vector<int>({6,2,4,5,6,1,55}));
	return 0;
}